db.fruits.insertMany([
    {
        name: "Apple",
        color: "Red",
        stocks: 20,
        price: 40,
        supplier_id: 1,
        onSale: true,
        origin: ["Philippines", "US"]
    },
    {
        name: "Banana",
        color: "Yellow",
        stocks: 15,
        price: 20,
        supplier_id: 2,
        onSale: true,
        origin: ["Philippines", "Ecuador"]
    },
    {
        name: "Kiwi",
        color: "Green",
        stocks: 25,
        price: 50,
        supplier_id: 1,
        onSale: true,
        origin: ["US", "China"]
    },
    {
        name: "Mango",
        color: "Yellow",
        stocks: 10,
        price: 120,
        supplier_id: 2,
        onSale: false,
        origin: ["Philippines", "India"]
    }
]);

// Using aggregate method
/*
    Syntax:
        db.collection.aggregate([]);
*/

/*
    "$match" method
    -Syntax:
        {$match: {field: value}}

    "$group" method
    -Syntax:
        {$group: {_id: "value"}, {fieldResult: "valueResult"}}
*/
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", total: {$sum : "$stocks"}}}
]);

// Field projection with aggregation
/*
    "$project"
    Syntax:
        include field = 1
        exclude field = 0
        {$project: {field: 1/0}}
*/

db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", total: {$sum: "$stocks"}}},
    {$project: {_id: 0}}
]);

// Sorting aggregated results (sort order ascending/descending)
/*
    "$sort"
    proper order 1
    reverse order -1

    Syntax:
        {$sort {field: 1/-1}}
*/
db.fruits.aggregate([
    {$match: {onSale: true}},
    {$group: {_id: "$supplier_id", total: {$sum: "$stocks"}}},
    {$sort: {total: -1}}
]);

// Aggregating results based on array fields
/*
    $unwind
    Syntax:
        {$unwind : field}
*/

db.fruits.aggregate([
    {$unwind: "$origin"}
]);

// Displays fruits documents by their origin and the kinds of fruits that are supplied

db.fruits.aggregate([
    {$unwind: "$origin"},
    {$group: {_id: "$origin", kinds: {$sum: 1}}}
]);

